import numpy as np

A=np.array([[1,-1,6],[-1,8,2],[6,2,1]])
b=np.array([23,30,22])
x0=np.array([0.0,0.0,0.0])
print(f"A = {A}")
print(f"b = {b}\n")
n=len(A) #Devuelve el tamaño de la primera dimensión de la matriz

#Técnicas de Pivoteo
for i in range(n-1):
    maxV=A[i,i]
    k=i
    for j in range(i+1,n):
        if abs(A[j,i])>abs(maxV):
            maxV=A[j,i]
            k=j
    if k!=i:
        tempA=np.copy(A[i,:]) #Guardamos Renglón
        A[i,:]=A[k,:]
        A[k,:]=tempA
        tempb=np.copy(b[i])
        b[i]=b[k]
        b[k]=tempb
print("Las nuevas matrices son: \n")
print(f"A = {A}")
print(f"b = {b}")

#Gauss-Seidel

maxIter=100 #Número máximo de iteraciones
epsilon=1*10**(-8) #Tolerancia

for k in range(maxIter):
    xi=np.copy(x0)
    for i in range(n):
        s=0
        for j in range(n):
            if j!=i:
                s=s+A[i,j]*xi[j]
        xi[i]=(1/A[i,i])*(b[i]-s)
    e=np.linalg.norm(xi-x0)
    print(f"x{k+1} = {xi}")
    if e<epsilon:
        break
    x0=np.copy(xi)
