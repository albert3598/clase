#include <stdio.h>
#include <time.h>
#include <stdlib.h>
 
int main() {
    srand(time(NULL));
    int i,j,k=0,rows,cols;
    // Se le pide al usuario el rango de la matriz
    printf(" Introduce un valor para los renglones de la matriz: ");
    scanf("%i",&rows);
    printf(" Introduce un valor para las columnas de la matriz: ");
    scanf("%i",&cols);
    int m1[rows*cols];// Para que sea un arreglo unidimensional se multiplicara los renglones por las columnas
    for(i = 0; i < rows*cols; i++) {
        m1[i] = rand() % 99; // Se llena la matriz con numeros aleatorios
    }
    printf("\t\nMATRIZ ORIGINAL\n");
    for(i = 0; i < rows*cols; i++) {
         
        if(i % cols == 0) // si es el ultimo elemento de la columna se imprime un salto de linea y se suma 1 a i porque daria un salto de linea
            
            printf("\n");
            printf("\t%i ",m1[i]);
       // sino la matriz se imprime en una sola linea
        
    }
    // Se genera la matriz transpuesta
    
    printf("\t\nMATRIZ TRANSPUESTA\n");
   
    
    for(i = 0; i < cols; i++) {
    		printf("\n");
        for(j = 0; j < rows; j++) //Se hace el acomodo de la matriz para hacer la matriz transpuesta
            
            printf("\t%d",m1[i+cols*j]);
        
	}
    
     
    
  
    return 0;
}
